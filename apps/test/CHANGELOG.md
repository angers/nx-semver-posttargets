# Changelog

This file was generated using [@jscutlery/semver](https://github.com/jscutlery/semver).

## [0.1.1](https://gitlab.com/angers/nx-semver-posttargets/compare/test-0.1.0...test-0.1.1) (2022-03-27)



# [0.1.0](https://gitlab.com/angers/nx-semver-posttargets/compare/test-0.0.2...test-0.1.0) (2022-03-27)


### Features

* test feat ([534414f](https://gitlab.com/angers/nx-semver-posttargets/commit/534414f200d76d095fc829b5acc9dc10caa15e87))



## [0.0.2](https://gitlab.com/angers/nx-semver-posttargets/compare/test-0.0.1...test-0.0.2) (2022-03-27)



## 0.0.1 (2022-03-27)


### Bug Fixes

* change test ([bff3e7f](https://gitlab.com/angers/nx-semver-posttargets/commit/bff3e7fe668995a596653134870a41e4f9b55a65))
* test app change ([0abd6c7](https://gitlab.com/angers/nx-semver-posttargets/commit/0abd6c7ded2f9988ee11564e1f990400f8e5be42))
* update test ([c7bf20e](https://gitlab.com/angers/nx-semver-posttargets/commit/c7bf20e0dbd670392649bf27fb835a90a154b9e5))
* update test app ([fb1cc27](https://gitlab.com/angers/nx-semver-posttargets/commit/fb1cc270d504d8ca55ad8979fa6999fe2975c85e))



## [0.0.4](https://gitlab.com/angers/nx-semver-posttargets/compare/test-0.0.3...test-0.0.4) (2022-03-26)


### Bug Fixes

* update test ([c7bf20e](https://gitlab.com/angers/nx-semver-posttargets/commit/c7bf20e0dbd670392649bf27fb835a90a154b9e5))



## [0.0.3](https://gitlab.com/angers/nx-semver-posttargets/compare/test-0.0.2...test-0.0.3) (2022-03-26)


### Bug Fixes

* change test ([bff3e7f](https://gitlab.com/angers/nx-semver-posttargets/commit/bff3e7fe668995a596653134870a41e4f9b55a65))



## [0.0.2](https://gitlab.com/angers/nx-semver-posttargets/compare/test-0.0.1...test-0.0.2) (2022-03-26)


### Bug Fixes

* test app change ([0abd6c7](https://gitlab.com/angers/nx-semver-posttargets/commit/0abd6c7ded2f9988ee11564e1f990400f8e5be42))



## 0.0.1 (2022-03-26)


### Bug Fixes

* update test app ([fb1cc27](https://gitlab.com/angers/nx-semver-posttargets/commit/fb1cc270d504d8ca55ad8979fa6999fe2975c85e))
